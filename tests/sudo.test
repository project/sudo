<?php

/**
 * @file
 * 
 * 
 */

class SudoTestCase extends DrupalWebTestCase {

  /**
   * Implementation of getInfo().
   */
  function getInfo() {
    return array(
      'name' => 'Sudo tests',
      'description' => 'test functionality of sudo form and menu item',
      'group' => 'Sudo'
    );
  }

  var $role_test = NULL;
  var $user_test = NULL;

  function setUp() {

    parent::setUp('sudo');

    $this->role_test = $this->drupalCreateRole(array('access administration pages', 'administer permissions', 'administer menu'));
    $this->user_test = $this->drupalCreateUser(array('access content'));
    user_multiple_role_edit(array($this->user_test->uid), 'add_role', $this->role_test);
    $this->drupalLogin($this->user_test);

  }

  function testSudo() {

    // test sudo administration page look
    $this->drupalGet('admin/user/sudo');
    $this->assertResponse(200, 'Sudo administration screen is visible');

    // test adding a user
    $edit = array('new_sudoer' => $this->user_test->name);
    $this->drupalPost('admin/user/sudo', $edit, t('Save'));
    $this->assertText($this->user_test->name, 'Add user to sudoers');

    // test adding roles to a user
    $edit = array("{$this->user_test->uid}[roles][{$this->role_test}]" => TRUE);
    $this->drupalPost('admin/user/sudo', $edit, t('Save'));
    $this->assertFieldChecked("edit-{$this->user_test->uid}-roles-{$this->role_test}", 'Add roles for sudoers');

    // test sudo form submission
    $this->assertTrue(db_result(db_query("SELECT uid FROM {users_roles} WHERE uid = %d AND rid = %d", $this->user_test->uid, $this->role_test)), 'Role is assigned to user');
    $this->drupalPost('admin/user/sudo', array(), t('sudo'));
    $this->assertFalse(db_result(db_query("SELECT uid FROM {users_roles} WHERE uid = %d AND rid = %d", $this->user_test->uid, $this->role_test)), 'Role has been removed from user');
    $this->assertRaw('normal-l.png', 'Sudo button disabled');

    // test sudo administration page access denied
    $this->assertResponse(403, 'Access to administration page is denied for users without "administer permissions"');

    // test sudo form submission
    $this->drupalPost('admin/user/sudo', array(), t('sudo'));
    $this->assertTrue(db_result(db_query("SELECT uid FROM {users_roles} WHERE uid = %d AND rid = %d", $this->user_test->uid, $this->role_test)), 'Role has been added to user');
    $this->assertRaw('sudoing-l.png', 'Sudo button enabled');

    // test hiding the button, changing names on button
    $edit = array(
      'sudo_hide_button' => TRUE,
      'sudo_title_sudoing' => 'sudo_title_sudoing',
      'sudo_title_normal' => 'sudo_title_normal'
    );
    $this->drupalPost('admin/user/sudo', $edit, t('Save'));
    $this->assertFieldByName('sudo_title_normal', 'sudo_title_normal', 'Button title saved for normal');
    $this->assertFieldByName('sudo_title_sudoing', 'sudo_title_sudoing', 'Button title saved for sudoing');
    $this->assertNoRaw('sudoing-l.png', 'Button hidden');
    $this->drupalPost('admin/user/sudo', array('sudo_hide_button' => FALSE), t('Save'));
    $this->assertRaw('sudoing-l.png', 'Button visible');

    // test changed names
    $this->drupalPost('admin/user/sudo', array(), t('sudo_title_sudoing'));
    $this->assertResponse(403, 'Button title rendered for sudoing');
    $this->drupalPost('admin/user/sudo', array(), t('sudo_title_normal'));
    $this->assertResponse(200, 'Button title rendered for normal');

    // test removing a user
    $edit = array("{$this->user_test->uid}[remove]" => TRUE);
    $this->drupalPost('admin/user/sudo', $edit, t('Save'));
    $this->assertNoRaw("<td>{$this->user_test->name}</td>", 'Remove user from sudoers');

  }

}
