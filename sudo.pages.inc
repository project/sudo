<?php

/**
 * @file
 */

function sudo_settings() {
  $form['help'] = array(
    '#value' => '<h2>' . t('Instructions') . '</h2><p>' .
                  t('Sudo module allows administrative users
                  to use the site in the same way that a normal user would, but
                  "sudo" as needed to obtain extra permissions for performing administrative tasks.'
                  . '</p><p>' .
                  'To use this module, you must specify administrative users below.  Those
                  users will then have access to a button on every page which will add or remove the site\'s
                  administrative roles on their account.  The roles to be added and removed can be configured on a
                  per-user basis with the form below.') . '</p>',
  );

  // Add user
  $form['new_sudoer'] = array(
    '#type' => 'textfield',
    '#title' => t('Add User'),
    '#description' => t('Type in a uid or part of a username.'),
    '#autocomplete_path' => 'user/autocomplete',
  );

  // User list
  $form['sudo_users'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sudo Users'),
    '#description' => t('Add users to this list using the form above.  After users are added,
                        specify a set of roles to be added or removed for each user when sudoing.'),
  );

  if (db_result(db_query("SELECT uid FROM {sudo}"))) {
    
    $q = db_query("SELECT * FROM {role}");
    while ($r = db_fetch_object($q)) {
      $roles[$r->rid] = check_plain($r->name);
    }
    $q = db_query("SELECT s.*, u.name FROM {sudo} s LEFT JOIN {users} u USING (uid)");
    while ($u = db_fetch_object($q)) {
      $sudo_roles = $u->roles ? unserialize($u->roles) : array();
      $form['sudo_users']['checkboxes'][$u->uid]['roles'] = array('#type' => 'checkboxes', '#options' => $roles, '#default_value' => $sudo_roles);
      $form['sudo_users']['checkboxes'][$u->uid]['name'] = array('#value' => check_plain($u->name));
      $form['sudo_users']['checkboxes'][$u->uid]['remove'] = array('#type' => 'checkbox', '#default_value' => FALSE);
      $form['sudo_users']['checkboxes'][$u->uid]['#tree'] = TRUE;
      $form['sudo_users']['checkboxes']['roles'] = array('#value' => $roles, '#tree' => TRUE);
      $form['sudo_users']['checkboxes']['#theme'] = 'sudo_checkboxes';
    }

  }

  // Sudo settings
  $form['button'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => 'Button Settings',
  );
  $form['button']['sudo_title_sudoing'] = array(
    '#type' => 'textfield',
    '#title' => t('Title when sudoing'),
    '#default_value' => variable_get('sudo_title_sudoing', 'sudo'),
  );
  $form['button']['sudo_title_normal'] = array(
    '#type' => 'textfield',
    '#title' => t('Title when normal'),
    '#default_value' => variable_get('sudo_title_normal', 'sudo'),
  );
  $form['button']['sudo_button_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Theme Path'),
    '#default_value' => variable_get('sudo_button_path', FALSE),
    '#description' => t('To change the images and/or css files, check this box and copy the "sudo_button" folder from the sudo module folder to your theme folder, then clear the cache and refresh the page.'),
  );
  $form['button']['sudo_hide_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Sudo Button'),
    '#default_value' => variable_get('sudo_hide_button', FALSE),
    '#description' => t('Check this box to disable display of the standard button.  You will have to implement the button in your own theme or in a php block.'),
  );

  // Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  drupal_add_css(drupal_get_path('module', 'sudo') . '/sudo_settings.css');
  return $form;
}

/**
 * Theme the administer permissions page.
 *
 * @ingroup themeable
 */
function theme_sudo_checkboxes(&$form) {

  $header[] = t('Users');
  foreach ($form['roles']['#value'] as $rid => $role_name) {
    $header[] = array('data' => $role_name, 'class' => 'centered');
  }
  $header[] = array('data' => t('Remove'), 'class' => 'centered remove');
  unset($form['roles']);

  $rows = array();

  foreach (element_children($form) as $uid) {
    // Don't take form control structures
    if (is_numeric($uid)) {
      $row = array();
      $row[] = array('data' => $form[$uid]['name']['#value']);
      // Module name
      foreach (element_children($form[$uid]['roles']) as $rid) {
        if (is_numeric($rid)) {
          $title = $form[$uid]['name']['#value'] . ' : ' . $form[$uid][$rid]['#title'];
          $form[$uid]['roles'][$rid]['#title'] = '';
          $row[] = array('data' => drupal_render($form[$uid]['roles'][$rid]), 'class' => 'checkbox', 'title' => $title);
        }
      }
      $row[] = array('data' => drupal_render($form[$uid]['remove']), 'class' => 'checkbox centered remove', 'title' => $form[$uid]['name']['#value'] . ' : ' . t('Remove'));
      unset($form[$uid]['name']);
      $rows[] = $row;
    }
  }
  $output = theme('table', $header, $rows, array('id' => 'permissions'));
  $output .= drupal_render($form);
  return $output;
}

function sudo_settings_validate($form, &$form_state) {
  if ($form_state['values']['new_sudoer'] && !db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $form_state['values']['new_sudoer']))) {
    form_set_error('new_sudoer', t('Not a valid username'));
  }
}

function sudo_settings_submit($form, &$form_state) {
  if ($uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $form_state['values']['new_sudoer']))) { // intentional assignment of $uid
    db_query("INSERT IGNORE INTO {sudo} (uid) VALUES (%d)", $uid);
  }
  foreach ($form_state['values'] as $uid => $values) {
    if ( is_array($form_state['values'][$uid]) && is_numeric($uid) ) {
      if ($values['remove']) {
        db_query("DELETE FROM {sudo} WHERE uid = %d", $uid);
      }
      else {
        $save_roles = serialize(array_values(array_filter($values['roles'])));
        db_query("REPLACE INTO {sudo} (uid, roles) VALUES (%d, '%s')", $uid, $save_roles);
      }
    }
  }
  foreach (array('sudo_hide_button', 'sudo_title_sudoing', 'sudo_title_normal', 'sudo_button_path') as $field) {
    variable_set($field, $form_state['values'][$field]);
  }
}

function sudo_switch() {
  $title = 'sudo_title_' . ($state = $_SESSION['sudoing'] ? 'sudoing' : 'normal');
  $path = variable_get('sudo_button_path', drupal_get_path('module', 'sudo'));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t(check_plain(variable_get($title, 'sudo'))),
  );
  return $form;
}

function sudo_switch_submit($form, &$form_state) {
  global $user;
  if ($sudo_roles = sudo_roles($user)) { // intentional assignment of $sudo_roles

    // if user is missing sudo roles, add them
    if (!$_SESSION['sudoing']) {
      foreach ($sudo_roles as $rid) {
        db_query("REPLACE INTO {users_roles} VALUES (%d, %d)", $user->uid, $rid);
      }
    }

    // otherwise, remove them
    else {
      $placeholders = db_placeholders($sudo_roles);
      array_unshift($sudo_roles, $user->uid);
      db_query("DELETE FROM {users_roles} WHERE uid = %d AND rid IN ($placeholders)", $sudo_roles);
    }

  }
}

/**
 * Returns a themed form for switching to and from sudo roles.
 * @param string $state either "sudoing" or "normal", used for display of
 * pictures surrounding the form's button
 * @param string $path the path to the themable elements and css files; by default,
 * either the module's directory or the theme's directory, depending on configuration
 * @return string html of a form with only a button
 */
function theme_sudo_button($state, $path) {
  $form = drupal_get_form('sudo_switch');
  $output .= "<style>div#sudo-button form input {background: transparent url(/$path/$state-c.png) repeat-x center;}</style>";
  $output .= "<div id='sudo-button' class='$state'><img src='/$path/$state-l.png'><img src='/$path/$state-r.png' class='sudo-img-r'>";
  $output .= $form;
  $output .= "</div>";
  return $output;
}
