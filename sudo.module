<?php

/**
 * @file
 */

/**
 * Implementation of hook_menu()
 */
function sudo_menu() {
  $items = array();
  $items['admin/user/sudo'] = array(
    'title' => 'Sudo Users',
    'description' => 'Settings for Sudo Users',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sudo_settings'),
    'access arguments' => array('administer permissions'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'sudo.pages.inc'
  );
  return $items;
}

function sudo_preprocess_page(&$vars) {
  global $user;
  if ($user->uid && !variable_get('sudo_hide_button', FALSE) && is_array($sudo_roles = sudo_roles())) { // intentional assignment of $sudo_roles
    $state = $_SESSION['sudoing'] ? 'sudoing' : 'normal';
    if (variable_get('sudo_button_path', FALSE)) {
      global $theme_path;
      $path = "$theme_path/sudo_button";
    }
    else {
      $path = drupal_get_path('module', 'sudo') . '/sudo_button';
    }
    drupal_add_css("$path/sudo_button.css");
    $vars['content'] .= theme('sudo_button', $state, $path);
    $vars['styles'] = drupal_get_css();
  }
}

/**
 * Fetch the current user's sudo roles.
 * @return array the roles that the current user
 */
function sudo_roles() {
  global $user;
  static $sudo_roles = array();
  if ($sudo_roles[$user->uid]) {
    return $sudo_roles[$user->uid];
  }
  if ($sudo_roles[$user->uid] = db_result(db_query("SELECT roles FROM {sudo} WHERE uid = %d", $user->uid))) { // intentional assignment of $roles
    $sudo_roles[$user->uid] = unserialize($sudo_roles[$user->uid]);
    $_SESSION['sudoing'] = !(count(array_diff($sudo_roles[$user->uid], array_keys($user->roles))));
  }
  return $sudo_roles[$user->uid];
}

function sudo_theme() {
  return array(
    'sudo_checkboxes' => array(
      'arguments' => array($form => NULL),
      'file' => 'sudo.pages.inc',
    ),
    'sudo_button' => array(
      'arguments' => array($state => NULL, $path => NULL),
      'file' => 'sudo.pages.inc',
    ),
  );
}
