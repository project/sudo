INSTALLATION

Copy the module's folder into your site's modules directory, and enable the module on your site.

ADMINISTRATION

Visit the administration page for the sudo module at /admin/user/sudo, and follow the directions for adding users and choosing which roles to toggle for their accounts.  Once any user has some roles enabled, a button will appear in the lower right corner of every page that user views.

CAVEATS

For simplicity, Sudo module simply toggles selected roles on a user's account; if (after the initial setup) you remove roles from a user's sudo roles, remove a user from sudoers entirely, or disable the module, that user will PERMANENTLY have whichever former sudo roles he or she currently has.  Therefore, whenever you do anything that results in removing roles from sudo settings, you will need to check the roles of all affected users to ensure that they have whatever roles they should have.  Adding roles does not present the same problem.

If you delete a user from the database entirely, their record in sudo table will not be removed until the next time you visit the sudo administration page.  This should not affect your site unless you manually add a new user with the same uid as a previously deleted user who had sudo privileges -- this would be a very strange thing to do.

THEMING

There are a few options on the administration page for theming the button.  You can set the button's text when sudoing and not sudoing; these values will NOT be translated.  To change the look of the button without writing code, check the "Use Theme Path" checkbox and follow the directions.  You can also override the function theme_sudo_button in a custom theme or module.  Finally, if you want the button not to be inserted into every page, check the "Hide Sudo Button" checkbox; you will then need to make the button display in your theme.
